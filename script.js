/* global $ */
/* global firebase */
/* global firebaseui */
/* global yourls */

$(function() {
  
  $( "#otherlabel" ).hide();
  $('#shortenLoader').hide();
  
  $( "input[type=radio][name=social]" ).change(function() {
    if($('#otherbtn').is(':checked')) { 
      $( "#otherlabel" ).show(100);
    } else {
      $( "#otherlabel" ).hide(100);
    }
    updateUrl();
  });
  
  $( "#basic-url, #campaignNameText, #campaignContentText, #othertext" ).keyup( updateUrl );
  
  $('#signOutBtn').click(function() {
    firebase.auth().signOut();
  });
  
  $('#shortenBtn').click(function() {
    var url = $("#longUrlDiv").text();
    console.log( 'Shortening: ', url);
    shorten(url);
  });
  
  initApp();
});

function updateUrl() {
  var social = $("input[name='social']:checked")[0].value == "otherbtn" ? $("#othertext").val() : $("input[name='social']:checked")[0].value;
  var basicUrl = processUrl($("#basic-url").val());
  var campaignName = processCampaign($("#campaignNameText").val());
  var campaignContent = processCampaign($("#campaignContentText").val());
  var genurl = "https://ethniqdiva.com" + basicUrl + "?utm_medium=social&utm_campaign=" + campaignName + "&utm_content=" + campaignContent + "&utm_source=" + social;
  $("#longUrlDiv").html(genurl);
}

function processCampaign(text) {
  text = text.replace(/[^a-zA-Z0-9]/g, '_').toLowerCase();
  return text;
}

function processUrl(url) {
  var pattern = /^(?:http[s]?:\/+)?(?:(?:www.)?ethniqdiva.com[\/]?)?/ig;
  var unsafe = /[^0-9a-zA-Z:\/\.\?\=;@&-]/gi;
  url = url.replace(pattern, '');
  url = url.replace(unsafe, '');
  url = url.replace(/\/?$/gi, '');
  
  $("#basic-url").val(url);
  
  return url =="" ? "/" : '/'+url+'/';
}

function shorten(url) {
  
  showAlert('clear');
  
  // Field validation
  if ($("#basic-url").val() == "" ) { showAlert('warning', 'Invalid input!', 'Empty website link'); return false}
  if ($("#campaignNameText").val() == "") { showAlert('warning', 'Invalid input!', 'Empty campaign name'); return false}
  if ($("#campaignNameText").val().length > 15) { showAlert('warning', 'Invalid input!', 'Campaign name too long'); return false}
  if ($("#campaignContentText").val().length > 15) { showAlert('warning', 'Invalid input!', 'Advert ID too long'); return false}
  if ( ($("input[name='social']:checked")[0].value == "otherbtn" ? $("#othertext").val() : $("input[name='social']:checked")[0].value) == "") { showAlert('warning', 'Invalid input!', 'Social Network not selected'); return false}
  
  $('#shortenLoader').show();
  $('#shortenBtn').prop('disabled', true);
  var keyword = $("#shortcode").val();
  var title = 'Social - ' + $("#campaignNameText").val() + ' - ' + $("#campaignContentText").val();
  
  yourls.shorten(url, {keyword: keyword, title: title}, (result, response) => {
    console.log('result: ', result);
    console.log('response: ', response);
    
    if (response.status == 'fail' && response.code == 'error:url' && response.message.endsWith('already exists in database')) {
      if (response.url.keyword == keyword) {
        showAlert('info', '(Nothing) Done!', 'The same url has been shortened with the same keyword before.');
        $("#shortUrlDiv").html(result.shorturl);
      } else {
        showAlert('danger', 'Failed!', 'This url has been shortened before with a different keyword.<br />The old shortened link is displayed.');
        $("#shortUrlDiv").html(result.shorturl);
      }
    }
    else if (response.status == 'fail' && response.code == 'error:keyword' && response.message.endsWith('already exists in database or is reserved')) {
      showAlert('danger', 'Failed!', 'This keyword is already in use for another long url.');
      $("#shortUrlDiv").html('');
    }
    else if (response.status == 'success') {
      database.ref('campaigns/'+ $("#campaignNameText").val() ).set(true);
      showAlert('success', 'Done!', 'Check the short url box below the long url box.');
      $("#shortUrlDiv").html(result.shorturl);
    }
    else {
      showAlert('danger', 'Unknown Error', 'Check console.');
      $("#shortUrlDiv").html('');
    }
    
    $('#shortenLoader').hide();
    $('#shortenBtn').prop('disabled', false);
  });
  
}

function showAlert(type='info', title='', message='') {
  if (type == 'clear') {
    $("#alertHolder").html('');
    return;
  }
  var alert = `
  <div class="alert alert-` + type + ` alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>` + title + `</strong> ` + message + `
  </div>`;
  $("#alertHolder").html(alert);
}


var campaigns, username, password;

// Firebase
var config = {
  apiKey: "AIzaSyDEMK_9SQfFC_mhl_BtQ118uJs6cqejF1Q",
  authDomain: "ethniqdiva-33d1f.firebaseapp.com",
  databaseURL: "https://ethniqdiva-33d1f.firebaseio.com",
  projectId: "ethniqdiva-33d1f",
  storageBucket: "ethniqdiva-33d1f.appspot.com",
  messagingSenderId: "507994753759"
};
firebase.initializeApp(config);
var database = firebase.database();

// FirebaseUI
var uiConfig = {
  callbacks: {
    signInSuccess: function(currentUser, credential, redirectUrl) {
      // Do something.
      // Return type determines whether we continue the redirect automatically
      // or whether we leave that to developer to handle.
        return false;
    }
  },
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    // firebase.auth.GithubAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID
  ],
  tosUrl: 'https://ethniqdiva.com/pages/terms-of-use'
};
var ui = new firebaseui.auth.AuthUI(firebase.auth());
// The start method will wait until the DOM is loaded.
ui.start('#firebaseui-auth-container', uiConfig);


var initApp = function() {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      $('#authDiv').hide();
      var displayName = user.displayName;
      var email = user.email;
      // var emailVerified = user.emailVerified;
      // var photoURL = user.photoURL;
      var uid = user.uid;
      var providerData = user.providerData;
      user.getToken().then(function(accessToken) {
        document.getElementById('sign-in-status').textContent = 'Signed in';
        document.getElementById('sign-in').textContent = 'Sign out';
        document.getElementById('account-details').textContent = JSON.stringify({
          displayName: displayName,
          email: email,
          // emailVerified: emailVerified,
          // photoURL: photoURL,
          uid: uid,
          // accessToken: accessToken,
          providerData: providerData
        }, null, '  ');
      });
      database.ref().child('campaigns').once('value', function(snap) {
        campaigns = Object.keys(snap.val());
        console.log('Campaigns for autocomplete', campaigns);
        $("#campaignNameText").typeahead({ source:campaigns, afterSelect: updateUrl });
      });
      database.ref().child('yourls').once('value', function(snap) {
        var data = snap.toJSON();
        username = data.username;
        password = data.password;
        yourls.connect('https://ediva.ga/yourls-api.php', {username: username, password: password}, {format: 'jsonp', method: 'GET'});
      });
    } else {
      // User is signed out.
      document.getElementById('sign-in-status').textContent = 'Signed out';
      document.getElementById('sign-in').textContent = 'Sign in';
      document.getElementById('account-details').textContent = 'null';
      $('#authDiv').show();
    }
  }, function(error) {
    console.log(error);
  });
};
